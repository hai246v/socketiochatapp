import http = require("http")

import staticfileserver = require("./staticfileserver")
import chatServer = require("./chat");

let webServer = http.createServer(staticfileserver.handler)
webServer.listen(8888)

chatServer.run(webServer)