import http = require("http")
import url = require("url")
import fs = require("fs")

export function handler(request: http.IncomingMessage, response: http.ServerResponse) {
    if (!request.url) throw new Error("request url can not be empty.")
    let pathname = url.parse(request.url).pathname
    if (!pathname) throw new Error("Path name can not be empty.")
    fs.readFile("./public/" + pathname, (error, data) => {
        if (error) {
            response.writeHead(404)
            response.end()
        } else {
            response.writeHead(200, { 'Content-Type': 'text/html' })
            response.write(data)
            response.end()
        }
    })
}

