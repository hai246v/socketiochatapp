import Server = require("socket.io")
import http = require("http")

export function run(server: http.Server) {
    let io = Server(server);
    io.of("/chat").on("connection", (socket) => {
        socket.on('join room', (data: any) => socket.join(data.roomName,
            () => io.of("/chat").in(data.roomName).emit("news", "new user joined " + socket.rooms[0])))
        socket.on('message', (data: any) => io.of("/chat").in(data.roomName)
            .emit("message", { message: data.message, user: data.user }))
    })


}