let socket = io.connect("http://" + window.location.host + "/chat")
socket.on("news", (data: any) => {
    console.log(data)
})

socket.on("message", (data: any) => {
    console.log(data)
    addMessage(data.user, data.message)
})


document.getElementById("chatArea").style.display = "None";

(<HTMLInputElement>document.getElementById("messageInput")).addEventListener("keydown", (ev) => {
    if (ev.key == "Enter")
        document.getElementById("sendButton").click()
})

function getUserName() {
    return (<HTMLInputElement>document.getElementById("nameInput")).value
}

function getRoomName() {
    return (<HTMLSelectElement>document.getElementById("roomNameInput")).value
}

function filterXSS(s: String) {
    return (s.replace(/>/g, "&gt;").replace(/</g, "&lt;"))
}

function addMessage(user: String, message: String) {
    let chatBody = document.getElementById("chatBody")
    chatBody.innerHTML += "<div class=\"row px-3\">"
        + (user == getUserName() ? "<div class=\"col-4\"></div>" : "")
        + "<div class=\"card mb-3 col-8 p-0\">"
        + (user != getUserName() ?
            "<div class=\"card-header p-1\"><h6>" :
            "<div class=\"card-header text-white bg-primary p-1\"><h6>")
        + filterXSS(user) + "</h6></div>"
        + "<div class=\"card-body\">" + filterXSS(message) + "</div>"
        + "</div></div>"
    chatBody.scrollTop = chatBody.scrollHeight
}

document.getElementById("enterButton").onclick = (ev) => {
    if (getUserName() == "") {
        document.getElementById("nameInput").classList.add("border-danger")
        return;
    }
    document.getElementById("loginArea").style.display = "None"
    document.getElementById("chatArea").style.display = ""
    document.getElementById("chatRoomName").innerText = getRoomName();
    (<HTMLInputElement>document.getElementById("nameDisplay")).value = getUserName()

    socket.emit("join room", { roomName: getRoomName() })
}

document.getElementById("sendButton").onclick = () => {
    let messageInput = <HTMLInputElement>document.getElementById("messageInput")
    socket.emit("message", { user: getUserName(), message: messageInput.value, roomName: getRoomName() })
    messageInput.value = ""
}
